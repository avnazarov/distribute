FROM tensorflow/tensorflow:2.3.1

RUN apt-get update && apt-get install -y \
        build-essential \
        cmake \
        g++-4.8

RUN pip install --upgrade pip matplotlib
RUN HOROVOD_WITH_TENSORFLOW=1 pip install --no-cache-dir horovod
RUN python -c "from tensorflow.keras import datasets; datasets.cifar10.load_data()"

ENTRYPOINT [ "horovodrun", "-np", "2", "-H", "localhost:2", "python", "/temp/train.py" ]
