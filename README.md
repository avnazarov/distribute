# Сборка и запуск
## Для сборки докера запустить ./build.sh
## Для запуска докера запустить ./run.sh

### Версии
На ветке master - tf(обучение через метод fit) + horovod<br>
На ветке func - tf(обучение через метод tf.function) + horovod<br>
На ветке tf-distritube - tf(обучение через метод fit) + tf.distritube

## Результат обучения
График обучения сохраняется в *accuracy.png*