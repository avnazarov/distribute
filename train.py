import tensorflow as tf
from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt
import horovod.tensorflow as hvd


def create_model():
    model = models.Sequential()
    model.add(layers.Conv2D(32, (3, 3), activation="relu", padding="same", input_shape=(32, 32, 3)))
    model.add(layers.MaxPool2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation="relu", padding="same",))
    model.add(layers.MaxPool2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation="relu", padding="same",))

    model.add(layers.Flatten())
    model.add(layers.Dense(64, activation="relu"))
    model.add(layers.Dense(10))
    return model

def save_result(history, path="accuracy.png"):
    plt.plot(history.history['accuracy'], label='accuracy')
    plt.plot(history.history['val_accuracy'], label = 'val_accuracy')
    plt.savefig(path)


def train():
    model = create_model()
    model.summary()

    (train_dataset, train_labels), (val_dataset, val_labels) = datasets.cifar10.load_data()
    train_dataset, val_dataset = train_dataset / 255. , val_dataset / 255.

    hvd.init()

    lr = 0.001 * hvd.size()

    opt = tf.optimizers.Adam(lr)
    opt = hvd.DistributedOptimizer(opt, backward_passes_per_step=1, 
                                average_aggregated_gradients=True)
    model.compile(
            loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
            optimizer=opt,
            metrics=["accuracy"],
            experimental_run_tf_function=False)

    callbacks = [
        hvd.keras.callbacks.BroadcastGlobalVariablesCallback(0),
        hvd.keras.callbacks.MetricAverageCallback(),
    ]

    if hvd.rank() == 0:
        callbacks.append(tf.keras.callbacks.ModelCheckpoint('./checpoint/checkpoint-{epoch}.h5'))

    history = model.fit(train_dataset, train_labels, epochs=3, batch_size=256,
                        validation_data=(val_dataset, val_labels), 
                        callbacks=callbacks, workers=2)

    save_result(history)


if __name__ == "__main__":
    train()